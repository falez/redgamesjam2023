import { _decorator, Component, Node, RigidBody, Vec3 } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('knockable')
export class knockable extends Component {

    @property(RigidBody)
    rb: RigidBody;

    @property
    itemId: string = '';

    knock(dir: Vec3) {

        const dirMult = dir.clone();
        dirMult.y += 0.1;
        dirMult.normalize();
        dirMult.multiplyScalar(20);
        this.rb.useGravity = true;
        this.rb.applyImpulse(dirMult);

        this.scheduleOnce(()=> {
            this.rb.useGravity = false;
            this.rb.clearState();
            this.rb.node.setPosition(Vec3.ZERO);
        }, 2.5)
    }
}


