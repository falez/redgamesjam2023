import { _decorator, Component, Node, Vec3 } from 'cc';
import { radar_house } from './radar_house';
import { spinturntest } from './car/spinturntest';
import { gameover_popup } from './ui/gameover_popup';
import { arrow_tet } from './ui/arrow_tet';
import { controller_drag } from './ui/controller_drag';
const { ccclass, property } = _decorator;

@ccclass('gameplay')
export class gameplay extends Component {

    @property(controller_drag)
    controller:controller_drag;

    @property(gameover_popup)
    resultPopUp: gameover_popup;

    @property(radar_house)
    houseRadar: radar_house;

    @property(radar_house)
    dropRadar: radar_house;

    @property(Node)
    pizza: Node;

    @property(Node)
    person: Node;

    @property(arrow_tet)
    indicator: arrow_tet;

    @property(Node)
    collectibleTest: Node;

    @property(spinturntest)
    car: spinturntest;

    score: number = 0;
    deliveryCount: number = 0;

    private isPickup: boolean = false;

    start() {
        this.collectibleTest.active = false;

        this.car.node.on('pickup', this.pickedUp, this);
        this.car.node.on('crash', this.crashed, this);

        this.resultPopUp.hide();
    }

    startGame() {
        this.scheduleOnce(this.spawnDrop.bind(this), 1.5);
        this.car.startGame();
        this.controller.startGame();
    }

    private crashed() {
        this.scheduleOnce(() => {
            this.resultPopUp.show(this.deliveryCount, this.score);
        }, 1.25);
    }

    private pickedUp() {

        this.isPickup = !this.isPickup;

        if (this.isPickup) {
            const v3 = new Vec3();
            if (this.dropRadar.tryGetDropPoint(v3)) {

                this.pizza.active = false;
                this.person.active = true;
                this.indicator.setLabel('Deliver');
                this.scheduleOnce(() => {
                    this.collectibleTest.setWorldPosition(v3);
                    this.collectibleTest.active = true;
                }, 0.25);
            }
        }
        else {
            this.score += 100;
            this.deliveryCount += 1;
            this.scheduleOnce(this.spawnDrop.bind(this), 0.5);
        }
    }

    private spawnDrop() {

        const v3 = new Vec3();
        if (this.houseRadar.tryGetDropPoint(v3)) {


            this.collectibleTest.setWorldPosition(v3);
            this.pizza.active = true;
            this.person.active = false;
            this.collectibleTest.active = true;
            this.indicator.setLabel('Pick\nUp');
        }
        else {
            this.scheduleOnce(this.spawnDrop.bind(this), 0.5);
        }
    }
}


