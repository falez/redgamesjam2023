import { _decorator, Color, Component, instantiate, math, Node, Prefab, Quat, random, randomRangeInt, Rect, Sprite, UITransform, Vec2, Vec3 } from 'cc';
const { ccclass, property } = _decorator;

const MIN_LEAF_SIZE = 3;
const MAX_LEAF_SIZE = 8;
const MAP_WIDTH = 30;
const MAP_HEIGHT = 30;
//const CELL_SIZE = 10;

class BuildingOrientations {

    private _yO;

    get yOrientation() {
        return this._yO;
    }

    constructor(u: boolean, r: boolean, d: boolean, l: boolean) {

        const possibleAngles: number[] = [];
        if (u) possibleAngles.push(0);
        if (l) possibleAngles.push(90);
        if (d) possibleAngles.push(180);
        if (r) possibleAngles.push(270);

        if (possibleAngles.length > 1) {
            const ran = randomRangeInt(0, possibleAngles.length);
            this._yO = possibleAngles[ran];
        }
        else
            this._yO = possibleAngles[0];
    }
}

class BSPNode {
    data: Rect;
    left: BSPNode;
    right: BSPNode;

    constructor(x: number, y: number, w: number, h: number) {
        this.data = new Rect(x, y, w, h);
    }

    // sugar
    isBranch() {
        return this.isSplit();
    }

    isSplit(): boolean {
        return this.left != null && this.right != null;
    }

    needSplit(maxSize: number): boolean {
        return this.data.width > maxSize || this.data.height > maxSize;
    }

    split(): boolean {

        if (this.isSplit()) return false;

        let horSplit = math.random() > 0.5;

        const { width, height, x, y } = this.data;

        if (width > height && (width / height) >= 1.25)
            horSplit = false;
        else if (height > width && (height / width) >= 1.25)
            horSplit = true;

        const max = (horSplit ? height : width) - MIN_LEAF_SIZE;

        if (max <= MIN_LEAF_SIZE)
            return false;

        const slicePoint = math.randomRangeInt(MIN_LEAF_SIZE, max);

        if (horSplit) {
            this.left = new BSPNode(x, y, width, slicePoint);
            this.right = new BSPNode(x, y + slicePoint, width, height - slicePoint);
        }
        else {
            this.left = new BSPNode(x, y, slicePoint, height);
            this.right = new BSPNode(x + slicePoint, y, width - slicePoint, height);
        }

        return true;
    }
}

@ccclass('mapgen')
export class mapgen extends Component {

    private bsp: BSPNode[] = [];

    @property(Prefab)
    road_h: Prefab;
    @property(Prefab)
    road_v: Prefab;
    @property(Prefab)
    road_crossroad: Prefab;

    @property([Prefab])
    road_bends: Prefab[] = [];
    @property([Prefab])
    road_inter3: Prefab[] = [];

    @property([Prefab])
    houses: Prefab[] = [];

    @property(Node)
    mapParent: Node;

    start() {

        const leaf = new BSPNode(0, 0, MAP_WIDTH, MAP_HEIGHT);
        this.bsp.push(leaf);

        let splitted = true;
        while (splitted) {
            splitted = false;

            for (let i = 0; i < this.bsp.length; i++) {
                const l = this.bsp[i];
                if (!l.isSplit()) {
                    if (l.needSplit(MAX_LEAF_SIZE) || math.random() > 0.25) {
                        if (l.split()) {
                            this.bsp.push(l.left);
                            this.bsp.push(l.right);
                            splitted = true;
                        }
                    }
                }
            }
        }

        this.makeRoads();
        this.makeMap();
        this.makeBuildings();

        this.drawMapDebug();

    }

    private drawMapDebug() {
        /*
        const col: Color[] = [
            Color.CYAN,
            Color.RED,
            Color.YELLOW,
            Color.MAGENTA
        ];

        for (let i = 0; i < this.leaves.length; i++) {
            const l = this.leaves[i];


            if (!l.data2) continue;

            const go = instantiate(this.prefabTest);
            go.parent = this.node;
            const spr = go.getComponent(Sprite);
            const uitrans = go.getComponent(UITransform);

            const { x, y } = l.data;
            const w = l.data.width * CELL_SIZE;
            const h = l.data.height * CELL_SIZE;
            uitrans.setAnchorPoint(0, 0);
            uitrans.setContentSize(w, h);
            const xoff = MAP_WIDTH / 2 * -CELL_SIZE;
            const yoff = MAP_HEIGHT / 2 * -CELL_SIZE;
            const xto = x * CELL_SIZE;
            const yto = y * CELL_SIZE;
            go.setPosition(xoff + xto, yoff + yto);

            const c = col[i % 4];
            spr.color = c;
            go.active = true;
        }
        */
    }

    private gridArr: number[][] = [];

    private makeRoads() {
        const gridArr = this.gridArr;

        // initialize empty
        for (let i = 0; i < MAP_HEIGHT; i++) {
            gridArr[i] = [];
            for (let j = 0; j < MAP_WIDTH; j++) gridArr[i][j] = 0;
        }

        const leaves = this.bsp.filter(x => !x.isBranch());
        for (let i = 0; i < leaves.length; i++) {

            const l = leaves[i];
            const { x, y, width, height } = l.data;

            for (let a = 0; a < width; a++)  gridArr[y][x + a] = 1;     // horizontal road
            for (let a = 0; a < height; a++) gridArr[y + a][x] = 2;     // vertical road
        }

        // force top and rightmost cell to be road
        for (let i = 0; i < MAP_HEIGHT; i++) gridArr[i][MAP_WIDTH - 1] = 2;
        for (let i = 0; i < MAP_WIDTH; i++) gridArr[MAP_HEIGHT - 1][i] = 1;

        // label junctions
        for (let i = 0; i < MAP_HEIGHT; i++) {
            for (let j = 0; j < MAP_WIDTH; j++) {
                if (gridArr[i][j] == 0) continue;

                // should use bitshift, no time, don't care.
                const u = i < MAP_HEIGHT - 1 && gridArr[i + 1][j] > 0 ? 1 : 0;
                const r = j < MAP_WIDTH - 1 && gridArr[i][j + 1] > 0 ? 2 : 0;
                const d = i > 0 && gridArr[i - 1][j] > 0 ? 4 : 0;
                const l = j > 0 && gridArr[i][j - 1] > 0 ? 8 : 0;
                gridArr[i][j] = u + r + d + l;
            }
        }
    }

    private buildingCoords: Vec3[] = [];

    private makeBuildings() {
        const gridArr = this.gridArr;

        for (let i = 1; i < MAP_HEIGHT - 1; i++) {
            for (let j = 1; j < MAP_WIDTH - 1; j++) {

                if (this.gridArr[i][j] == 0) {

                    const u = gridArr[i + 1][j] > 0;
                    const r = gridArr[i][j + 1] > 0;
                    const d = gridArr[i - 1][j] > 0;
                    const l = gridArr[i][j - 1] > 0;

                    if (u || r || d || l) {
                        const bO = new BuildingOrientations(u, r, d, l);

                        this.buildingCoords.push(new Vec3(j,0,i));

                        const ran = randomRangeInt(0, this.houses.length);
                        const go = instantiate(this.houses[ran]);
                        go.parent = this.mapParent;
                        const pos = new Vec3((j * 6) + 3, 0, (i * -6) - 3);
                        go.setPosition(pos);
                        go.setRotationFromEuler(0, bO.yOrientation);

                    }
                }
            }
        }

    }

    private makeMap() {

        for (let i = 0; i < MAP_HEIGHT; i++) {
            for (let j = 0; j < MAP_WIDTH; j++) {
                if (this.gridArr[i][j] == 0) continue;

                let pf: Prefab;
                let q: Quat = new Quat();
                switch (this.gridArr[i][j]) {
                    default:
                    case 10: pf = this.road_h; break;
                    case 5: Quat.fromEuler(q, 0, 90, 0); pf = this.road_v; break;

                    case 3: pf = this.road_bends[0]; break;
                    case 6: pf = this.road_bends[1]; break;
                    case 9: pf = this.road_bends[2]; break;
                    case 12: pf = this.road_bends[3]; break;

                    case 7: pf = this.road_inter3[0]; break;
                    case 11: pf = this.road_inter3[1]; break;
                    case 13: pf = this.road_inter3[2]; break;
                    case 14: pf = this.road_inter3[3]; break;

                    case 15: pf = this.road_crossroad; break;
                }

                const pos = new Vec3((j * 6), 0, (i * -6));
                const scale = new Vec3(1, 1, 1);
                const go = instantiate(pf);
                go.parent = this.mapParent;
                go.setRTS(q, pos, scale);
            }
        }
    }
}


