import { _decorator, Camera, Component, Label, Mat3, math, Node, Quat, UITransform, Vec2, Vec3, view } from 'cc';
const { ccclass, property } = _decorator;

const v3r = new Vec3(0, 1, 0);
const v3unclmp = new Vec3(0);
@ccclass('arrow_tet')
export class arrow_tet extends Component {

    @property(Camera)
    mainCam: Camera;

    @property(UITransform)
    relativeTo: UITransform;

    @property(Label)
    label: Label;


    @property(Node)
    pivot: Node;

    @property(Node)
    indicator: Node;

    @property(Node)
    indicatorArrow: Node;

    @property(UITransform)
    uitrans: UITransform;

    private v3: Vec3 = new Vec3();

    setLabel(str: string) {
        this.label.string = str;
    }

    private updateRotation() {
        const v3 = this.v3;
        const q = new Quat();

        const uitrans = this.uitrans;
        if (uitrans.getBoundingBox().contains(new Vec2(v3unclmp.x, v3unclmp.y))) {
            Quat.fromAngleZ(q, -180);
        }
        else {

            const dir = v3.clone().subtract(this.pivot.position).normalize();
            const theta = Math.acos(Vec3.dot(dir, v3r)) * -Math.sign(dir.x);


            Quat.fromMat3(q, new Mat3(
                Math.cos(theta), Math.sin(theta), 0,
                -Math.sin(theta), Math.cos(theta), 0,
                0, 0, 1
            ))
        }




        this.indicatorArrow.setRotation(q);
    }

    setIndicator() {
        let v3 = this.v3;
        const wp = this.node.worldPosition.clone();


        wp.y += 3;
        this.mainCam.convertToUINode(wp, this.relativeTo.node, v3unclmp);

        const uitrans = this.uitrans;
        if (uitrans.getBoundingBox().contains(new Vec2(v3unclmp.x, v3unclmp.y))) {
            v3unclmp.y += 20;
        }

        const screenDim = this.relativeTo.contentSize;

        const v3min = new Vec3(-screenDim.x / 2 + 100, -screenDim.y / 2 + 320, 0);
        const v3max = new Vec3(screenDim.x / 2 - 100, screenDim.y / 2 - 100, 0);

        v3.set(v3unclmp);



        v3.clampf(v3min, v3max);

        this.indicator.setPosition(v3);
        this.updateRotation();
    }

    protected onEnable(): void {

        this.setIndicator();
        this.indicator.active = true;
    }

    protected onDisable(): void {
        this.indicator.active = false;
    }

    update(deltaTime: number) {

        this.setIndicator();
    }
}


