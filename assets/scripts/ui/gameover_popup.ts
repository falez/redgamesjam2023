import { _decorator, Component, director, Label, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('gameover_popup')
export class gameover_popup extends Component {

    @property(Label)
    deliveryCountLabel: Label;
    @property(Label)
    scoreCountLabel: Label;

    show(deliveries: number, score: number) {
        this.scoreCountLabel.string = score.toString();
        this.deliveryCountLabel.string = deliveries.toString();

        this.node.active = true;
    }

    hide() {
        this.node.active = false;
    }

    onClickReplay() {

        director.loadScene('scene');
    }

    onClickMenu() {

    }
}


