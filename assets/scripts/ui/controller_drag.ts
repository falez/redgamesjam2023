import { _decorator, Component, director, easing, EventTouch, game, Input, lerp, math, Node, tween, UITransform, Vec2, Vec3 } from 'cc';
const { ccclass, property } = _decorator;

const center: Vec2 = new Vec2();
const touchPoint: Vec2 = new Vec2();
const touchPointV3: Vec3 = new Vec3();
const dt: Vec2 = new Vec2();

const ABC = {
    lastChangeTime: 0
}

class BT {
    dtx: number;
}



export const ControllerData = {
    dtnorm: 0,
    to: 0
}

@ccclass('controller_drag')
export class controller_drag extends Component {

    private aa: number;
    private a: number;

    @property(Node)
    steeringWheel: Node;

    @property(UITransform)
    relativeTo: UITransform;

    @property(Node)
    guide: Node;

    private showGuide: boolean = true;

    start() {

        this.guide.active = false;
        let nodePos = this.node.position;
        center.set(nodePos.x, nodePos.y);

    }

    startGame()
    {
        this.scheduleOnce(()=> {
            
            this.guide.active = true;
        
            this.node.on(Input.EventType.TOUCH_START, this.onTouchStart, this);
            this.node.on(Input.EventType.TOUCH_MOVE, this.onTouchMove, this);
            this.node.on(Input.EventType.TOUCH_END, this.onTouchEnd, this);
            this.node.on(Input.EventType.TOUCH_CANCEL, this.onTouchEnd, this);
        
        }, 1.5);
    }


    protected onEnable(): void {

    }

    protected onDisable(): void {
        this.node.off(Input.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.off(Input.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.off(Input.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.off(Input.EventType.TOUCH_CANCEL, this.onTouchEnd, this);

    }

    private onTouchStart(evt: EventTouch) {


        if (this.showGuide) {
            this.showGuide = false;
            this.guide.active = false;
        }


        evt.getUILocation(touchPoint);

        touchPointV3.set(touchPoint.x, touchPoint.y, 0);
        this.relativeTo.convertToNodeSpaceAR(touchPointV3, touchPointV3);

        touchPoint.set(touchPointV3.x, touchPointV3.y);
        center.set(touchPoint);
        Vec2.subtract(dt, touchPoint, center);
        dt.x = math.clamp(dt.x, -100, 100);
        let dtnorm = dt.x / 100;

        ABC.lastChangeTime = game.frameStartTime / 1000;
        ControllerData.to = dt.x;

        /*
                let bt = new BT();
                bt.dtx = 0;
                let tw = tween(bt);
                let dur = 0.1 * (Math.abs(dtnorm));
                tw.to(dur, { dtx: dt.x }, {
                    easing: 'quadOut', onUpdate: (a: BT) => {
        
                        let pos = this.steeringWheel.position.clone();
                        pos.x = a.dtx;
                        this.steeringWheel.setPosition(pos);
        
                        ControllerData.dtnorm = a.dtx / 100;
                    }
                });
                tw.start();
        */
    }

    private onTouchMove(evt: EventTouch) {

        evt.getUILocation(touchPoint);
        touchPointV3.set(touchPoint.x, touchPoint.y, 0);
        this.relativeTo.convertToNodeSpaceAR(touchPointV3, touchPointV3);

        touchPoint.set(touchPointV3.x, touchPointV3.y);
        Vec2.subtract(dt, touchPoint, center);
        dt.x = math.clamp(dt.x, -100, 100);
        let dtnorm = dt.x / 100;

        ABC.lastChangeTime = game.frameStartTime / 1000;
        ControllerData.to = dt.x;

        /*

        this.a = dtnorm;


        this.relativeTo.convertToNodeSpaceAR(touchPointV3, touchPointV3);
        touchPointV3.y = 0;
        this.steeringWheel.setPosition(touchPointV3);
        */

    }

    private onTouchEnd(evt: EventTouch) {

        ControllerData.to = 0;
        dt.set(0, 0);
    }

    private kk: number = 0;

    private accel: number = 0;
    private str: number = 10;

    update(deltaTime: number) {

        let to = ControllerData.to / 100;
        let strPs = this.str * deltaTime;

        if (to != 0) {
            let sign = Math.sign(to);
            this.accel += strPs * sign;
            this.accel = math.clamp(this.accel, -to, to);
        }
        else {
            let sign = Math.sign(this.accel);

            this.accel -= strPs * sign;

            if (Math.sign(this.accel) != sign) {
                this.accel = 0;
            }
        }

        ControllerData.dtnorm = -this.accel;

        //this.steeringWheel.setPosition(this.accel * 100, 0, 0);
    }
}


