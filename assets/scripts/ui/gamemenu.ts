import { _decorator, Component, Node } from 'cc';
import { gameplay } from '../gameplay';
const { ccclass, property } = _decorator;



@ccclass('gamemenu')
export class gamemenu extends Component {

    static firstTime:boolean = true;

    @property(gameplay)
    theGame:gameplay;

    start() {
        if(!gamemenu.firstTime)
        {
            this.playGame();
        }
    }
    
    playGame()
    {
        this.theGame.startGame();
        this.node.active = false;
        gamemenu.firstTime = false;
    }
}


