import { _decorator, AudioSource, Collider, Component, easing, EventTouch, game, Input, ITriggerEvent, Label, math, Node, ParticleSystem, PhysicsGroup, Quat, RigidBody, tween, Vec2, Vec3 } from 'cc';
import { ControllerData } from '../ui/controller_drag';
import { knockable } from '../knockable';
const { ccclass, property } = _decorator;

const v3_0: Vec3 = new Vec3();

@ccclass('spinturntest')
export class spinturntest extends Component {

    @property(Node)
    rotPivot: Node;

    @property(RigidBody)
    rb: RigidBody;

    private vel: Vec3 = new Vec3();

    @property
    stop: boolean = true;
    
    @property(Node)
    cameraNode: Node;

    start() {

        const c = this.node.getComponent(Collider);

        c.on('onTriggerEnter', this.onTriggerEnter, this);
        //this.rb.setLinearVelocity(this.vel);
    }

    startGame() {
        
        this.stop = false;
        this.vel.set(this.rotPivot.forward.clone().multiplyScalar(10));

        const pos = this.cameraNode.position.clone();

        pos.y = 5;

        const tw = tween(this.cameraNode);
        tw.to(1.0, {position:pos}, {easing: 'quadOut'});

        tw.start();
    }

    private onTriggerEnter(event: ITriggerEvent) {


        if (event.otherCollider.getGroup() == 16) {
            console.log(event.otherCollider.node.name);
            event.otherCollider.node.active = false;

            this.node.emit('pickup');
        }
        else if (event.otherCollider.getGroup() == 32) {

            const comp = event.otherCollider.getComponent(knockable);

            if(comp)
            {
                const dir = event.otherCollider.node.worldPosition.clone();
                dir.subtract(this.node.worldPosition);
                dir.normalize;
                comp.knock(dir);
                this.node.emit('knock', comp.itemId);
            }
        }
        else {

            this.node.emit('crash');
            console.log(event.type, event);

            this.vel.set(0, 0, 0);

            const rot = Quat.toEuler(new Vec3(), this.rotPivot.rotation);
            this.rotPivot.setRotationFromEuler(-10, rot.y, 0);
            const tw = tween(this.rotPivot);

            tw.to(1.0, { rotation: new Quat() }, { easing: 'elasticOut' });
            tw.start();
            this.stop = true;
        }
    }

    @property(AudioSource)
    audioSource_tireSqueal: AudioSource;

    @property([ParticleSystem])
    ps_squeals: ParticleSystem[] = []

    update(deltaTime: number) {

        if (this.stop) {

            this.vel.set(0, 0, 0);

            if (this.audioSource_tireSqueal.playing)
                this.audioSource_tireSqueal.stop();

            // if (this.audioSource_engine.playing)
            //     this.audioSource_engine.stop();
            return;
        }
        // let sino = Math.sin(game.frameStartTime / 1000)*0.5 + 0.5;

        // if (!this.audioSource_engine.playing)
        //     this.audioSource_engine.play();
        let q = new Quat();

        const degreeTurn = ControllerData.dtnorm * 20;

        const audioVolume = math.clamp((Math.abs(ControllerData.dtnorm) - 0.45) / (0.75 - 0.45), 0, 1) * 0.2;

        this.audioSource_tireSqueal.volume = audioVolume;
        if (Math.abs(degreeTurn) > 9) {
            if (!this.audioSource_tireSqueal.playing)
                this.audioSource_tireSqueal.play();
        }
        else {
            if (this.audioSource_tireSqueal.playing)
                this.audioSource_tireSqueal.stop();

        }

        if (Math.abs(degreeTurn) > 12) {
            this.ps_squeals.forEach(element => {
                element.play();
            });
        }
        else {
            this.ps_squeals.forEach(element => {
                element.stopEmitting();
            });
        }

        Quat.rotateY(q, q, math.toRadian(degreeTurn));
        this.rotPivot.setRotation(q);

        let q2 = this.node.rotation.clone();

        Quat.rotateY(q2, q2, ControllerData.dtnorm * math.toRadian(100 * deltaTime));

        this.node.setRotation(q2);


        //let horForce = ControllerData.dtnorm * -2*deltaTime;
        //const horAxis = this.rotPivot.right.clone();
        //horAxis.multiplyScalar(horForce);

        //this.rb.getLinearVelocity(v3_0);
        //this.vel.set(v3_0);
        //this.vel.add(horAxis);

        // this.rb.setLinearVelocity(this.node.forward.clone().multiplyScalar(1))
        this.node.translate(this.vel.clone().multiplyScalar(deltaTime));

    }
}


