import { _decorator, BoxCollider, Collider, Component, Eventify, ITriggerEvent, Label, Node, Vec3 } from 'cc';
import { shuffle } from './array_utils';
const { ccclass, property } = _decorator;

@ccclass('radar_house')
export class radar_house extends Eventify(Component) {

    private collidersInRange: Record<string, Collider> = {};

    @property(Label)
    label:Label;

    start() {

        const triggers = this.getComponentsInChildren(BoxCollider);

        triggers.forEach(c => {
            c.on('onTriggerEnter', this.onTriggerEnter, this);
            c.on('onTriggerExit', this.onTriggerExit, this);
        });
    }

    tryGetDropPoint(out: Vec3):boolean {
        
        const keys = Object.keys(this.collidersInRange);
        if(keys.length == 0) return false;

        shuffle(keys);

        out.set(this.collidersInRange[keys[0]].node.worldPosition);

        return true;
    }

    private onTriggerEnter(event: ITriggerEvent) {

        this.collidersInRange[event.otherCollider.uuid] = event.otherCollider;
    }

    private onTriggerExit(event: ITriggerEvent) {

        delete this.collidersInRange[event.otherCollider.uuid];
    }

    protected update(dt: number): void {
        
        const keys = Object.keys(this.collidersInRange);

        if(this.label)
        this.label.string = keys.toString();
    }
}


